$(function(){
	$('.dropdown').hover(function() {
	$(this).addClass('open').find('.dropdown-menu').first().stop(true, true).show();
    }, function() {
	$(this).removeClass('open').find('.dropdown-menu').first().stop(true, true).hide();
    });

    if ($(window).width() < 768) {
    	$('iframe#ic-calculator-iframe[style]').attr('scrolling', 'yes');
    }

    $(document).on('click', '.sticky-sidebar a[href^=\\#]', function(e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top }, 500, 'linear');
    });

    var window_width = $(window).width();
    if (window_width < 768) {
        $(".sticky-sidebar").trigger("sticky_kit:detach");
    } else {
        if ($('.sticky-sidebar').length) {
            make_sticky();
        }
    }
    $(window).resize(function() {
        window_width = $(window).width();
        if (window_width < 768) {
            $(".sticky-sidebar").trigger("sticky_kit:detach");
        } else {
            if ($('.sticky-sidebar').length) {
                make_sticky();
            }
        }
    });

    function make_sticky() {
        $('.sticky-sidebar').stick_in_parent({
            'parent': $('.js-sticky-container'),
            'offset_top': 30
        });
    }
});