<?php
/**
 * Main function file for blank wordpress theme.
 * @package blankwordpresstheme
 */

function bwt_menu() {
  register_nav_menus(array(
      'header-menu' => __( 'Header Menu' )
    )
  );
}
add_action('init', 'bwt_menu');

function bwt_styles() {
    wp_enqueue_style('bwt-bootstrap-style', get_template_directory_uri() . '/assets/css/bootstrap.min.css');
    wp_enqueue_style('bwt-jasny-style', get_template_directory_uri() . '/assets/css/jasny-bootstrap.min.css');
    wp_enqueue_style('bwt-styles', get_template_directory_uri() . '/assets/css/main.min.css');
    wp_enqueue_script('bwt-script', get_template_directory_uri() . '/assets/js/jquery-3.1.0.min.js');
    wp_enqueue_script('bwt-bootstrap-script', get_template_directory_uri() . '/assets/js/bootstrap.min.js');
    wp_enqueue_script('bwt-jasny-script', get_template_directory_uri() . '/assets/js/jasny-bootstrap.min.js');
    wp_enqueue_script('bwt-sticky-kit', get_template_directory_uri() . '/assets/js/jquery.sticky-kit.min.js');
    wp_enqueue_script('bwt-scripts', get_template_directory_uri() . '/assets/js/main.js', array(), '0.0.1', true );
}

add_action('wp_enqueue_scripts', 'bwt_styles');

function wp_pagination() {
global $the_query;
$big = 12345678;
$page_format = paginate_links( array(
    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
    'format' => '?paged=%#%',
    'current' => max( 1, get_query_var('paged') ),
    'total' => $the_query->max_num_pages,
    'type'  => 'array'
) );
  if( is_array($page_format) ) {
              $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
              echo '<li>'. $paged . ' of ' . $the_query->max_num_pages .'</li>';
              foreach ( $page_format as $page ) {
                echo "<li>$page</li>";
              }
  }
}
/*** Theme Customizer area. ***/
require get_template_directory() . '/functions/customizer.php';
/*** Register widget area.***/
require get_template_directory() . '/functions/widgets.php';
require get_template_directory() . '/functions/walkermenu.php';
require get_template_directory() . '/widgets/widget-footer-menu.php';
require get_template_directory() . '/widgets/custom-widget-footer-menu.php';
require get_template_directory() . '/widgets/widget-footer-description.php';
require get_template_directory() . '/widgets/widget-footer-taxonomy.php';