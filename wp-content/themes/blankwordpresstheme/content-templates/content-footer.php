<?php
/**
 * Section footer file for blank wordpress theme.
 * @package blankwordpresstheme
 */
?>
<footer id="bwt-footer">
		<div class="footer-links">
				<div class="container">
						<div class="row">
								<?php dynamic_sidebar('bwt-footer'); ?>
						</div>
				</div>
		</div>
</footer>
