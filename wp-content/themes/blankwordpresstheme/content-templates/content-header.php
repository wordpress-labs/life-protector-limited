<?php
/**
 * Section header file for blank wordpress theme.
 * @package blankwordpresstheme
 */
?>

<div id="bwt-header">
		<div class="bwt-header-holder">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 visible-xs">
						<div class="header-top">
							<?php dynamic_sidebar('bwt-header-top'); ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4">
						<div class="bwt-logo">
				             <a href="<?php bloginfo('url');?>">
				               <?php if(get_theme_mod('bwt_logo')): ?>
				               <img src="<?php echo get_theme_mod('bwt_logo');?>" alt="<?php echo esc_attr(get_bloginfo('name', 'display'));?>" class="img-responsive" />
				               <?php else: ?>
				                 Blank Wordpress Theme
				              <?php endif; ?>
				             </a>
		           		</div>
		           		<button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
				            <span class="sr-only">Toggle navigation</span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				        </button>
					</div>
					<div class="col-sm-8">
						<div class="header-top">
							<?php dynamic_sidebar('bwt-header-top'); ?>
						</div>
						<nav id="bwt-navigation" class="navbar">
							<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-animations">
								<?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container' => 'div', 'container_class' => '', 'container_id' => '', 'menu_class' => 'nav nav-menu hidden-xs','walker'=> new wp_bootstrap_navwalker()) ); ?>
							</div>
						</nav>
					</div>
				</div>	
			</div>
		</div>
</div>

<div class="navmenu navmenu-default navmenu-fixed-left offcanvas">
    <?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container' => 'div', 'container_class' => '', 'container_id' => '', 'menu_class' => 'nav nav-menu visible-xs mobile-menu','walker'=> new wp_bootstrap_navwalker()) ); ?>
</div>