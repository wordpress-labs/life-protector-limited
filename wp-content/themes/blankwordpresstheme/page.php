<?php
/**
 * Main header file for blank wordpress theme.
 * @package blankwordpresstheme
 */
?>
<!DOCTYPE html>
<html lang="en">
<html <?php language_attributes(); ?>>
<head>
<title>
  	<?php bloginfo('name');?> |
  	<?php is_front_page() ? bloginfo('description') : wp_title(''); ?>
</title>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-title" content="<?php bloginfo('name'); ?> - <?php bloginfo('description'); ?>">
<meta name="format-detection" content="telephone=no">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
<?php wp_head(); ?>
</head>

<body <?php if( is_front_page() || is_home()) : echo 'id="bwt-homepage"'; else: echo 'id="bwt-singlepage"'; endif; ?>>
<?php get_template_part('content-templates/content', 'header'); ?>

<div class="pf-blog">
    <div class="pf-container">
        <div class="pf-section">
            <div class="col-sm-8 col-xs-12">
                <div class="pf-blog content wrapper">
                      <?php
                        $categories = get_the_category();
                        if(is_archive()){
                          $category_id = '0';
                        }
                        if(is_category()){
                          $category_id = $categories[0]->cat_ID;
                        }
                        $url_link = "//".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                        $url_path = parse_url($url_link);
                        $url = explode('/', $url_path['path']);
                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        $args = array(
                          'paged' => $paged,
                          'orderby' => 'date',
                          'order' => 'DESC' ,
                          'cat' => $category_id,
                          'year' => $url[2],
									        'monthnum' => $url[3],
                          'posts_per_page' => 5,
                          'paged'=>$paged,
                          'post_status' => 'publish'
                      );
                      ?>
                      <?php $the_query = new WP_Query( $args ); ?>
                      <?php $i=1; while ($the_query->have_posts()): $the_query->the_post();?>
                      <div class="pf-blog content post wrap" data-id="<?php echo $i;?>">
                        <div class="pf-section">
                            <div class="col-xs-12">
                              <div class="pf-blog content post holder">
                                <span class="pf-blog content post date">
                                  <div class="day"><?php echo get_the_date('d');?></div>
                                  <div class="month"><?php echo get_the_date('M y');?></div>
                                </span>
                                <span class="pf-blog content post title">
                                  <h2><a href="<?php get_the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                  <h5><?php the_category(); ?></h5>
                                </span>
                              </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="pf-blog content post image">
                                   <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
                                    <?php if($thumb): ?>
                                    <a href="<?php the_permalink(); ?>"><div class="pf-main-img" style="background-image: url('<?php echo $thumb['0'];?>')"></div></a>
                                    <?php else: ?>
                                    <a href="<?php the_permalink(); ?>"><div class="pf-main-img" style="background-image: url('<?php bloginfo("template_directory")?> <?php echo '/assets/img/blog-img.jpg';?>')"></div> </a>
                                    <?php endif;?>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="pf-blog content post text">
                                    <?php the_excerpt(); ?>
                                    <a href="<?php the_permalink(); ?>">Read More</a>
                                </div>
                            </div>
                        </div>
                      </div>
                      <?php $i++; endwhile;?>
                </div>
                <div class="pf-blog pagination-holder">
                      <ul class="pf-blog pagination-holder pagination">
                          <li>
                            <?php
                            global $the_query;
                            $big = 999999999;
                            echo paginate_links( array(
                                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                'format' => '?paged=%#%',
                                'current' => max( 1, get_query_var('paged') ),
                                'total' => $the_query->max_num_pages,
                                'prev_text' => __('<i class="fa fa-angle-left" aria-hidden="true"></i>'),
                                'next_text' => __('<i class="fa fa-angle-right" aria-hidden="true"></i>')
                            ) );
                            ?>
                          </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4 col-xs-12">
                <div class="pf-blog sidebar">
                    <?php dynamic_sidebar('bwt-sidebar'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
