<?php
/**
 * Main footer template file for blank wordpress theme.
 * @package blankwordpresstheme
 */
?>

<?php get_template_part('content-templates/content', 'footer'); ?>
<?php wp_footer(); ?>
</body>
</html>
