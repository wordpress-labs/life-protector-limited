<?php
/**
 * Main template file for blank wordpress theme.
 * @package blankwordpresstheme
 * Template Name: About
 */

get_header(); ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
        <?php if($thumb): ?>
            <div class="pf-inner-banner" style="background-image: url('<?php echo $thumb['0'];?>')"></div>
        <?php else: ?>
            <div class="pf-inner-banner" style="background-image: url('<?php bloginfo("template_directory")?> <?php echo '/assets/img/blog-img.jpg';?>')"></div>
        <?php endif;?>
    <div class="main container">     
        <?php the_content(); ?>
    </div>    
    <?php endwhile;?>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
