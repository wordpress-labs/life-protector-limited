<?php
/**
 * Main header file for blank wordpress theme.
 * @package blankwordpresstheme
 */
get_header(); ?>

<div class="pf-blog">
    <div class="pf-container">
        <div class="pf-section">
          <div class="col-sm-4 col-sm-push-8 col-xs-12">
                <div class="pf-blog sidebar">
                    <?php dynamic_sidebar('bwt-sidebar'); ?>
                </div>
          </div>
          <div class="col-sm-8 col-sm-pull-4 col-xs-12">
            <?php while ( have_posts() ) : the_post(); ?>


              <div class="pf-blog content wrapper">
                  <div class="pf-blog content post wrap">
                      <div class="pf-section">
                          <div class="col-xs-12">
                              <div class="pf-blog content post holder">
                                  <span class="pf-blog content post date">
                                    <div class="day"><?php echo get_the_date('d');?></div>
                                    <div class="month"><?php echo get_the_date('M y');?></div>
                                  </span>

                                  <span class="pf-blog content post title">
                                    <h2><?php the_title(); ?></h2>
                                    <h5><?php the_category();?></h5>
                                  </span>
                              </div>
                          </div>


                          <div class="col-xs-12">
                              <div class="pf-blog content post image">
                                <?php the_post_thumbnail('full', array('class' =>'img-responsive')); ?>
                              </div>
                              <div class="pf-blog content post text single">
                                  <?php the_content(); ?>
                              </div>
                          </div>
                      </div>

                      <div class="pf-section">
                          <div class="pf-blog content-share">
                              <div class="col-xs-12">
                              </div>
                          </div>
                      </div>
                      <?php endwhile;?>

                      <div class="pf-section">
                          <?php
                          $related = new WP_Query((array(
                            'category__in' => wp_get_post_categories($post->ID),
                            'posts_per_page' => 3,
                            'post__not_in' => array($post->ID)
                          )));

                          if( $related->have_posts() ) { ?>
                            <div class="col-xs-12">
                                <div class="pf-blog related holder">
                                    <h3>Related Posts</h3>
                                </div>
                            </div>
                          <?php while( $related->have_posts() ) {
                              $related->the_post();
                              echo '<div class="col-sm-4 col-xs-12">';
                              echo'<div class="pf-blog related thumbs">';
                          ?>
                          <?php $thumb =   wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
                          <?php if($thumb): ?>
                          <a href="<?php the_permalink(); ?>"><div class="pf-main-img" style="background-image: url('<?php echo $thumb['0'];?>')"></div></a>
                          <?php else: ?>
                          <a href="<?php the_permalink(); ?>"><div class="pf-main-img" style="background-image: url('<?php bloginfo("template_directory")?> <?php echo '/assets/img/blog-img.jpg';?>')"></div> </a>
                          <?php endif;?>
                              <a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a>
                          <?php
                              echo'</div>';
                              echo'</div>';
                              }
                          }
                          wp_reset_postdata();
                          ?>
                      </div>
                  </div>
              </div>
          </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>

