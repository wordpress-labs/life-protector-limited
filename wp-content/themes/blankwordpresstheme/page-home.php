<?php
/**
 * Main template file for blank wordpress theme.
 * @package blankwordpresstheme
 * Template Name: Homepage
 */

get_header(); ?>
    <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>
    <div class="floating-bg">
        <div class="floating-bg-wrap" style="background: url('<?php echo $backgroundImg[0]; ?>')"></div>
    </div>
    <div class="main container">
        <?php while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile;?>
    </div>
<?php get_footer(); ?>
