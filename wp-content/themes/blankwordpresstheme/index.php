<?php
/**
 * Main header file for blank wordpress theme.
 * @package blankwordpresstheme
 */
get_header(); ?>

<div class="pf-blog">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-push-8 col-xs-12">
                <div class="pf-blog sidebar">
                    <?php dynamic_sidebar('bwt-sidebar'); ?>
                </div>
            </div>
            <div class="col-sm-8 col-sm-pull-4 col-xs-12">
                <div class="pf-blog content wrapper">
                      <?php
                      	$url_link = "//".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                        $url_path = parse_url($url_link);
                        $url = explode('/', $url_path['path']);
                        $categories = get_the_category();
                        if(is_archive()){
                          $category_id = '0';
                        }
                        if(is_category()){
                          $category_id = get_cat_ID(ucwords(str_replace('-', ' ', $url[3])));
                      	}
                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        $args = array(
                          'paged' => $paged,
                          'orderby' => 'date',
                          'order' => 'DESC' ,
                          'cat' => $category_id,
                          'year' => $url[2],
						  'monthnum' => $url[3],
                          'posts_per_page' => 5,
                          'paged'=>$paged,
                          'post_status' => 'publish'
                      );
                      ?>
                      <?php $the_query = new WP_Query( $args ); ?>
                      <?php while ($the_query->have_posts()): $the_query->the_post();?>
                      <div class="pf-blog content post wrap">
                        <div class="row">
                            <div class="col-xs-12">
                              <div class="pf-blog content post holder">
                                <span class="pf-blog content post date">
                                  <div class="day"><?php echo get_the_date('d');?></div>
                                  <div class="month"><?php echo get_the_date('M y');?></div>
                                </span>
                                <span class="pf-blog content post title">
                                  <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                  <h5><?php the_category(); ?></h5>
                                </span>
                              </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="pf-blog content post image">
                                    <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
                                    <?php if($thumb): ?>
                                    <a href="<?php the_permalink(); ?>"><div class="pf-main-img" style="background-image: url('<?php echo $thumb['0'];?>')"></div></a>
                                    <?php else: ?>
                                    <a href="<?php the_permalink(); ?>"><div class="pf-main-img" style="background-image: url('<?php bloginfo("template_directory")?> <?php echo '/assets/img/blog-img.jpg';?>')"></div> </a>
                                    <?php endif;?>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="pf-blog content post text">
                                    <?php the_excerpt(); ?>
                                    <a href="<?php the_permalink(); ?>">Read More</a>
                                </div>
                            </div>
                        </div>
                      </div>
                      <?php endwhile;?>
                </div>
                <div class="pf-blog pagination-holder">
                      <ul class="pf-blog pagination-holder pagination">
                          <li>
                            <?php
                            global $the_query;
                            $big = 999999999;
                            echo paginate_links( array(
                                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                'format' => '?paged=%#%',
                                'current' => max( 1, get_query_var('paged') ),
                                'total' => $the_query->max_num_pages,
                                'prev_text' => __('<i class="fa fa-angle-left" aria-hidden="true"></i>'),
                                'next_text' => __('<i class="fa fa-angle-right" aria-hidden="true"></i>')
                            ) );
                            ?>
                          </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
