<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'lifeprotectorlimited');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '%yO|CZ2uj}ajV^v{w,Pp:.n^d:jMkPNBLu)yk7(c;v-S9b1sQ5.:VC(<Lx@<muvB');
define('SECURE_AUTH_KEY',  ';>k?(OGVillZyb[.S11W<[PrUe7]x*T0gR-1jS0XT}_!DdlvP8pt)85{^5hvGmX=');
define('LOGGED_IN_KEY',    'VolMa!,^&K3:Y#n4^?l 0FiW;c#S@&2GW_WDV[vC2&PZ;nH7efnr_`6_p%,Tm=8[');
define('NONCE_KEY',        'ZqB!0p6xWHGZX$l5.ivF !Lx~8<X;)0$p7vC0Q70O)$7o_)LL0[*mg7>GxXMBMEP');
define('AUTH_SALT',        '~}AVzH!*/%,M>;%WJk<~F67eX3NKg=qE0I@dD#D.0-U;7?rh_E(ces_zrJ//0R,V');
define('SECURE_AUTH_SALT', 'F.r*M#TFF`1@(B%>b_uwYrUc)!<@|o1.@IwdB)sF4uAUif|fpZ(O(ry:i+.0xo`;');
define('LOGGED_IN_SALT',   'B0xIdLAJy)7Ao=qdP^oW#uo7uvZT8w%*/8GM$!C>m(B?l4y$+21Q^MGiy1AdAdrA');
define('NONCE_SALT',       'TjxCi,znC8rCr@YSuWoc9d1)h@S0Y/ej?Z]$;#X :Y[<<U^v1 s%&N3d=00k/iBR');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
